﻿namespace DevApi.Challenge.Models
{
    public class Developer
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Squad { get; set; } = string.Empty;
        public string Login { get; set; } = string.Empty;
        public string Avatar { get; set; } = string.Empty;
        public string? Tag { get; set; } = string.Empty;
        public DateTime? DtCreation { get; set; }
        public DateTime? CreatedAt { get; set; }

    }
}
