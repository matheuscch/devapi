﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace DevApi.Challenge.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) 
        {
            //Database.EnsureCreated();
        }
        public DbSet<Developer> Developers { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=TempProsoftDevTest;Integrated Security=true");
            optionsBuilder.UseSqlServer(@"Server=deploydbdev.database.windows.net;Database=deploydbdev;User Id=dbazure;Password=1233Mm!!;");
        }
        
    }
}
