<h1 align = "center"> Desafio Alterdata/Prosoft: Dev Api </h1>
<h1 align ="center">
    <img src="https://i.imgflip.com/7jz6fo.gif">
</h1>

---

# Indice
- [Sobre](#-sobre)
- [Tecnologias Utilizadas](#-tecnologias-utilizadas)
- [Dependências](#-dependencias)
- [Validação](#validação-de-email-no-domínio)
- [Developer](#-developer)

## Deploy
A aplicação está publicada na azure disponível >[aqui](https://devapichallenge.azurewebsites.net/)<

## ☔ Sobre

O projeto **Dev Api** é uma tabela dinâmica para visualizar dados. Os dados são criados através das rotas de requisição GET, POST, UPDATE e DELETE (CRUD) e mantidos em SQL server.

---

## 🔧 Tecnologias Utilizadas

A arquitetura escolhida para o projeto foi criada no padrão Aplicativo Web MVC, com o Scaffold Controller, utilizando as seguintes tecnologias:
- [ASP.NET Core](https://reactstrap.github.io/)
- [EntityFramework]()
- [SqlServer]()

---

## 🚩 Dependências

### Foram escolhidas estas bibliotecas para ORM
```bash
    $ PM> EntityFrameworkCore
    $ PM> EntityFrameworkCore.Design
    $ PM> EntityFrameworkCore.SqlServer
    $ PM> EntityFrameworkCore.Tools
```
---
## ✔ Validação de Email no Domínio
#### A rota cria com validação para descartar alterações que não apresentam o domínio de email adequado.
```C#
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Email,Squad,Login,Avatar,Tag,DtCreation,CreatedAt")] Developer developer)
        {
            if(!developer.Email.Contains("@prosoft"))
            {
                throw new Exception("Prosoft Email Domain is not valid");
            }

            if (ModelState.IsValid)
            {
                _context.Add(developer);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(developer);
        }
```

---

## 🔍 Developer

Na paginação "Table", pode ser encontrado um campo de filtro e cadastro **Post New**.Também é possível acessar individualmente o campo através do
Exemplo:
```
   Selecionar um dev específico -> Details View
```
 <img src="https://i.imgflip.com/7jz6fo.gif">
 

---
 
---

Desenvolvido por Matheus Carvalho